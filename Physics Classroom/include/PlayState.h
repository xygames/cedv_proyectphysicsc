/*********************************************************************
 * 	Curso de Experto en Desarrollo de Videojuegos
 * Autor: Francisco Javier Morales Hidalgo franmorales93@gmail.com
 * Autor: José Antonio Pérez Florencia japflorencia@gmail.com 
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#define LADRILLO 1 << 1

#include <Ogre.h>
#include <OIS/OIS.h>
#include <vector>
#include <sstream>
#include <utility>
#include "GameState.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}

  void enter ();
  void createScene();
  

  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  void winstate();
  void losestate();

 // bool hayladrillos();
  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  Ogre::SceneNode * _node;

  Ogre::OverlayManager* _overlayManager;
  Ogre::Overlay *_overlay;
  Ogre::Real deltaT;

  Ogre::RaySceneQuery *_raySceneQuery;

  Ogre::Entity* ball;
  Ogre::SceneNode* nodeball;
  Ogre::Entity* pala;
  Ogre::SceneNode* nodepala;
  Ogre::SceneNode * _selectedNode;

  std::vector<Ogre::SceneNode*> _ladri; 
  std::vector<std::vector<Ogre::SceneNode*> >  _ladriM;



  Ogre::Vector3 vdx;
  Ogre::Vector3 vdz;
  Ogre::Vector3 vdxpala;
  Ogre::Vector3 v;
  Ogre::Vector3 p1;
  Ogre::Vector3 p2;
  Ogre::Vector3 p3;
  Ogre::Vector3 p4;


  int choque;
  int dir;
  int frame;
  int life;
  int ladrillos;
  int gameReady;
  int puntuacion;
  int totalLadrillos;
  Ogre::Real alpha;


  Ogre::Ray Rayo;





  SoundFXManager* _pSoundFXManager;
  SoundFXPtr _palaEffect;
  SoundFXPtr _ladrilloEffect;

  Ogre::uint32 mask;
  bool _exitGame;
 
 
 private:

  OIS::MouseState _mouse_position;
  bool _key_pressed;

  std::stringstream _s;
  Ogre::Real _time_count,_last_time;
  Ogre::OverlayElement *oe;
  Ogre::OverlayElement *ue;
};

#endif
