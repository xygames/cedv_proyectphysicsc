#include "PlayState.h"
#include "PauseState.h"
#include <OgreStringConverter.h>


template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{
  totalLadrillos = 0;
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(0,100,0));
  _camera->lookAt(Ogre::Vector3(0,02,-02));

  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);

  _node = _sceneMgr->getRootSceneNode()->createChildSceneNode("P");
    
  _pSoundFXManager = SoundFXManager::getSingletonPtr();
  _palaEffect = _pSoundFXManager->load("Pala.wav");
  _ladrilloEffect = _pSoundFXManager->load("Ladrillo.wav");

  _viewport = _root->getAutoCreatedWindow()->getViewport(0);
  // Nuevo background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

  // Crear info.
  _overlayManager = Ogre::OverlayManager::getSingletonPtr();
  _overlay = _overlayManager->getByName("Info");
  _overlay->show();

  // Ocultar logo Game Over
  oe = _overlayManager->getOverlayElement("logoGO");
  oe->hide();

  // Ocultar overlay Fase Superada
  oe = _overlayManager->getOverlayElement("logoClear");
  oe->hide();
 
  oe = _overlayManager->getOverlayElement("instrucciones");
  oe->show();

  _last_time = _time_count = 0;  

  createScene();
  _raySceneQuery = _sceneMgr->createRayQuery(Ogre::Ray());  

  _exitGame = false;
  frame = 0;
  alpha =1.005;



  CEGUI::MouseCursor::getSingleton().hide( );
  puntuacion = 0;
  ladrillos = 0;
  gameReady = -1;

}


//CREANDO LA ESCENA
void PlayState::createScene() {
  vdx = Ogre::Vector3(0.59,0,0);
  vdz = Ogre::Vector3(0,0,-0.7);
  vdxpala = Ogre::Vector3(0,0,0);

  life=3;

  _ladri.reserve(10);
  _ladriM.reserve(5);





  //-------------------------Suelo----------------------------

  Ogre::Plane plane1(Ogre::Vector3::UNIT_Y, 1);
  Ogre::MeshManager::getSingleton().createPlane("plane1", 		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 		plane1,240,140,1,1,true,1,1,1,Ogre::Vector3::UNIT_Z);
  Ogre::SceneNode* node2 = _sceneMgr->createSceneNode("ground");
  Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt",
	"plane1");
  node2->setScale(1,1,1);
  groundEnt->setMaterialName("ground");
  node2->attachObject(groundEnt);
   _node->addChild(node2);



  //------------------------Bola--------------------------------
  ball = _sceneMgr->createEntity("ball.mesh");
  nodeball = _sceneMgr->createSceneNode("ballNode");
  nodeball->attachObject(ball);
  _node->addChild(nodeball);
  nodeball->translate(20,1,0);







  //------------------------Pala--------------------------------
  pala = _sceneMgr->createEntity("pala.mesh");
  nodepala = _sceneMgr->createSceneNode("palaNode");
  nodepala->attachObject(pala);
  _node->addChild(nodepala);
  nodepala->setScale(5,1,1);
  nodepala->translate(10,1,20);

  //---------------------Ladrillos------------------------------
  Ogre::SceneNode * _node3;
  std::stringstream ladrilloName;
  std::string s = "LADRILLO";
  for (int i=0;i < 5;i++){
    for (int j=0;j < 10;j++){
      //base
      ladrilloName << s << totalLadrillos;
      totalLadrillos = totalLadrillos+1;
      Ogre::Entity* ent1 = _sceneMgr->createEntity(ladrilloName.str(),"cubo.mesh");
      ent1->setQueryFlags(LADRILLO);
      _node3 = _sceneMgr->createSceneNode();
      _node3->attachObject(ent1);
      _node3->setPosition(-15 + j*5,0,-25 + i*3);;
      _node3->scale(2.5,1,1);
      _node->addChild(_node3);
      ladrilloName.str("");
      _ladri.push_back(_node3);
      //_nodes.push_back(_node3);
    }
  _ladriM.push_back(_ladri);
  _ladri.clear();
  } 


  mask = LADRILLO;

}


void
PlayState::exit ()
{
  std::cout << "EXIT PLAYSTATE"<<std::endl;
  //_s.str(" ");
  _overlay->hide();

  CEGUI::MouseCursor::getSingleton().show( );


  //_root->getAutoCreatedWindow()->removeAllViewports();
  //_sceneMgr->clearScene();
  _sceneMgr->destroyEntity("planeEnt");
  std::stringstream ladrilloName;
  std::string s = "LADRILLO";
  for (int i=0;i < totalLadrillos;i++){
    ladrilloName << s << i;
    _sceneMgr->destroyEntity(ladrilloName.str());
    ladrilloName.str("");
   }
  //_sceneMgr->destroySceneNode("ground");
  _sceneMgr->destroySceneNode("ballNode");
  _sceneMgr->destroySceneNode("palaNode");
  //_overlayManager->destroyAll();
  _node->removeAndDestroyAllChildren();
  _sceneMgr->destroySceneNode(_node);


  //Ogre::SceneNode* node2 = _sceneMgr->createSceneNode("ground");
  //Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt","plane1");

  //_sceneMgr->clearScene();
  //_node->removeAllChildren();


  //Ogre::MaterialManager::getSingleton().remove("Background");

  //_root->getAutoCreatedWindow()->removeAllViewports();
  

}

void
PlayState::pause()
{
 _overlay->hide();
}

void
PlayState::resume()
{
  _overlay->show();
  // Se restaura el background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}
//////////////////////////////////////////////

/////////////////////////////////////////////
bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
  if(gameReady==1){
  //Time Control Overlay
  deltaT = evt.timeSinceLastFrame;
  oe = _overlayManager->getOverlayElement("timeinf");
  _last_time += deltaT;
  //_s.str("");
  //_s << std::setprecision(1)<<std::fixed << _last_time;
  oe->setCaption(Ogre::StringConverter::toString(_last_time));

  //Points Control Overlay
  
  puntuacion = ladrillos *100;
  oe = _overlayManager->getOverlayElement("minesinf");
  oe->setCaption(Ogre::StringConverter::toString(puntuacion));

  //Lifes Control Overlay

  oe = _overlayManager->getOverlayElement("fpsInfo");
  oe->setCaption(Ogre::StringConverter::toString(life));
  //oe = _overlayManager->getOverlayElement("Instrucciones");
  //oe->show();

if(frame>0){
	frame = frame - 1;
}else{

  Ogre::Vector3 P[3];
  Ogre::Vector3 p1(-1,0,-0.5);
  Ogre::Vector3 p2(1,0,-0.5);
  Ogre::Vector3 p3(-1,0,0.5);
  Ogre::Vector3 p4(1,0,0.5);
 



////////COMPRUEBA SI HAY BLOQUE 
  Ogre::Ray Rayo(nodeball->getPosition()+1.5*(vdx+vdz), vdx+vdz);  
  _raySceneQuery->setRay(Rayo);
  _raySceneQuery->setSortByDistance(true);
  _raySceneQuery->setQueryMask(mask);

  Ogre::RaySceneQueryResult &result= _raySceneQuery->execute(); 
  Ogre::RaySceneQueryResult::iterator it;

  
    //std::cout<< "Lanza rayo"<<std::endl;  

  it=result.begin();


  if (it!=result.end() && it->movable->getQueryFlags()== LADRILLO){
    _selectedNode=it->movable->getParentSceneNode();

   // std::cout<< "Da"<<std::endl;
      Ogre::AxisAlignedBox aab = _selectedNode->_getWorldAABB().intersection(nodeball->_getWorldAABB());
    if (aab.isNull()){

       std::cout<< "No corta"<<std::endl;

      p1= _selectedNode->getPosition()+ p1;
      p2= _selectedNode->getPosition()+ p2;
      p3= _selectedNode->getPosition()+ p3;
      p4= _selectedNode->getPosition()+ p4;

      P[0]= p1;
      P[1]= p2;
      P[2]= p3;
      P[3]= p4;
      v=Ogre::Vector3(100000,100000,100000);
      for(int y=0;y<3;y++){
      for (int i= 0 ; i<4; i++){
        //  std::cout << "Hola"<<std::endl;        
        if(nodeball->getPosition().squaredDistance(P[i])
	<nodeball->getPosition().squaredDistance(v)){
          v=P[i];
          choque=i;
         // std::cout << i<<std::endl;
	  } 
      }
}
      v=v - nodeball->getPosition();
      if ((v.z*v.z) / (v.x*v.x) < (vdz.z*vdz.z) / (vdx.x*vdx.x)){
      dir=2;}
      else if ((v.z*v.z) / (v.x*v.x) > (vdz.z*vdz.z) / (vdx.x*vdx.x)){
      dir=1;}
      else if ((v.z*v.z) / (v.x*v.x) == (vdz.z*vdz.z) / (vdx.x*vdx.x)){
      dir=0;}      
    }  
    else{
    frame=0;

      switch (choque)
        {case 0:
          std::cout<<"Vetice sup izq"<<std::endl;
          if (dir==1){
      	  vdz= (-1)*vdz;
          //std::cout<<"Arriba"<<std::endl;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  
	  winstate();        
          }
          else if (dir==2){
      	  vdx= (-1)*vdx;
          //std::cout<<"Izquierda"<<std::endl;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();        
          }
          else if (dir == 0) {
          //std::cout<<"Pico"<<std::endl;
          vdx= (-1)*vdx;
      	  vdz= (-1)*vdz;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
   	  winstate();        
          }
          break; 
        
        case 1:
          //std::cout<<"Vetice sup der"<<std::endl;
          if (dir==1){
          //std::cout<<"Arriba"<<std::endl;
      	  vdz= (-1)*vdz;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();        
          }
          else if (dir==2){
          //std::cout<<"Derecha"<<std::endl;
      	  vdx= (-1)*vdx;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();         
          }
          else if (dir == 0){
          //std::cout<<"Izquierda"<<std::endl;
          vdx= (-1)*vdx;
      	  vdz= (-1)*vdz;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();
          }
          break;            
        case 3:
          //std::cout<<"Vetice inf izq"<<std::endl;
          if (dir==1){
          //std::cout<<"Abajo"<<std::endl;
      	  vdz= (-1)*vdz;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();        
          }
          else if (dir==2){
          //std::cout<<"Izquierda"<<std::endl;
      	  vdx= (-1)*vdx;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();        
          }
          else if (dir == 0){
          //std::cout<<"Pico"<<std::endl;
          vdx= (-1)*vdx;
      	  vdz= (-1)*vdz;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();
          }
          break;    
        case 2:
          //std::cout<<"Vetice inf der"<<std::endl;
          if (dir==1){
          //std::cout<<"Abajo"<<std::endl;
      	  vdz= (-1)*vdz;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();
          }
          if (dir==2){
          //std::cout<<"Derecha"<<std::endl;
          vdx= (-1)*vdx;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();
          }
          if (dir == 0){
          //std::cout<<"Pico"<<std::endl;
          vdx= (-1)*vdx;
      	  vdz= (-1)*vdz;
	  _selectedNode->detachAllObjects();
	  _ladrilloEffect->play();
	  winstate();        
          }
          break;    
        }    
  	
  
    }
  }
  else {
  _selectedNode=0;
  //std::cout << "No da el rayo "<<std::endl;
  }
}
/////LA BOLA SE MUEVE 
  
  nodeball->translate(vdx+vdz);
  
//Si llega al borde derecho, rebota
  if (nodeball->getPosition().x>40 && vdx.x>0){
  vdx=(-1)*vdx;
  nodeball->translate(vdx+vdz);
  

  }
//Si llega al borde izquierdo, rebota 
  else if (nodeball->getPosition().x<-20 && vdx.x<0){
  vdx=(-1)*vdx;
  nodeball->translate(vdx+vdz);
  }






//Si llega al borde inferior, rebota
  if (nodeball->getPosition().z>30 && vdz.z>0){
    life=life-1;
    gameReady = -1;

    if (life==0) losestate();
  vdx = Ogre::Vector3(0.59,0,0);
  vdz = Ogre::Vector3(0,0,-0.7);
    nodeball->setPosition(20,1,0);



  }//Si llega al borde superior, rebota
  else if (nodeball->getPosition().z<-30 && vdz.z<0){
  vdz=(-1)*vdz;
  nodeball->translate(vdx+vdz);



  }//Si colisiona con la pala, rebota
  else if (nodeball->getPosition().z>18.5 && nodeball->getPosition().z<20 && vdz.z>0 && 
nodeball->getPosition().x < nodepala->getPosition().x+7 && 	nodeball->getPosition().x > nodepala->getPosition().x-7){
    vdz=(-1)*vdz;
    _palaEffect->play();
    
    }
  
  //Se mueve la pala
  nodepala->translate(vdxpala);
  if(nodepala->getPosition().x<=-17.5)
    nodepala->translate(-vdxpala);
  if(nodepala->getPosition().x>=37.5)
    nodepala->translate(-vdxpala);    


}
  return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P) {
    pushState(PauseState::getSingletonPtr());
  }
   else{
 
    if(e.key == OIS::KC_LEFT){
    if(gameReady == -1){
        oe = _overlayManager->getOverlayElement("instrucciones");
  	oe->hide(); 
	gameReady = 1;
    }
    vdxpala = vdxpala + Ogre::Vector3(-1,0,0);
    }
    if(e.key == OIS::KC_RIGHT){
    if(gameReady == -1){
        oe = _overlayManager->getOverlayElement("instrucciones");
  	oe->hide();
	gameReady = 1;
    }
    vdxpala = vdxpala + Ogre::Vector3(1,0,0);
    }
}
  if (e.key == OIS::KC_ESCAPE) {
  gameReady=0;

  }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
    popState();
  }
  else if (e.key == OIS::KC_LEFT){
  vdxpala = vdxpala - Ogre::Vector3(-1,0,0);
  }
  else if(e.key == OIS::KC_RIGHT){
  vdxpala = vdxpala - Ogre::Vector3(1,0,0);
  } 
/*  else if (e.key == OIS::KC_UP){
 Ogre::ManualObject* manual = _sceneMgr->createManualObject("manual"); manual->begin("BaseWhiteNoLighting",Ogre::RenderOperation::OT_LINE_STRIP);   manual->position(Rayo.getOrigin()); // start position 

manual->position(Rayo.getDirection()*1234500000000); 
   
manual->end(); 
_sceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manual);


  }*/

}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void PlayState::losestate(){
  oe = _overlayManager->getOverlayElement("fpsInfo");
  oe->setCaption(Ogre::StringConverter::toString(life));

      Ogre::OverlayElement * oe = _overlayManager->getOverlayElement("logoGO");
  oe->show();
  gameReady=0;
}

void PlayState::winstate(){
    ladrillos = ladrillos + 1;
    vdx=alpha*vdx;
    vdz=alpha*vdz;
    if (ladrillos==50){
	Ogre::OverlayElement* oe = _overlayManager->getOverlayElement("logoClear");
    oe->show();
    gameReady=0;
    puntuacion = static_cast<int>(puntuacion + (1000000/_last_time));

  std::string puntosFinal = Ogre::StringConverter::toString(puntuacion);
  std::ofstream file("records.txt", std::ofstream::app | std::ofstream::out);
  file << "Jugador " << puntosFinal << std::endl;
  file.close();
	

     }
}


