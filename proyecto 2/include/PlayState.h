/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include "GameState.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);
  void createscene();
  void Abrotrampilla();
  void DetectCollision();
  void Explotoglobo();
  void RecargoFlecha();
  void ClavarFlecha();
  void lose();

  // Heredados de Ogre::Singleton.
  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  OgreBulletDynamics::DynamicsWorld * _world;
  OgreBulletCollisions::DebugDrawer * _debugDrawer;
//Rigids bodies and Shapes
  OgreBulletCollisions::CollisionShape *ShapePlane;
  OgreBulletDynamics::RigidBody *rigidBodyPlane;
  OgreBulletCollisions::CollisionShape *Rock1Shape;
  OgreBulletDynamics::RigidBody *Rock1rigidBody;           
  OgreBulletCollisions::CollisionShape *Rock2Shape;
  OgreBulletDynamics::RigidBody *Rock2rigidBody;
  OgreBulletCollisions::CollisionShape *Rock3Shape;
  OgreBulletDynamics::RigidBody *Rock3rigidBody;
  OgreBulletCollisions::CollisionShape *BaseShape;
  OgreBulletDynamics::RigidBody *BaserigidBody;
  OgreBulletCollisions::CollisionShape *BotonShape;
  OgreBulletDynamics::RigidBody *BotonrigidBody;

  OgreBulletDynamics::RigidBody *EntradarigidBody;
  OgreBulletDynamics::RigidBody *rigidflecha;

  OgreBulletCollisions::CollisionShape *RampaShape;
  OgreBulletDynamics::RigidBody *RamparigidBody; 
  OgreBulletCollisions::CollisionShape *BarrilShape;
  OgreBulletDynamics::RigidBody *BarrilrigidBody; 
  OgreBulletCollisions::CollisionShape *BaseTramShape;
  OgreBulletDynamics::RigidBody *BaseTramrigidBody;
  OgreBulletCollisions::CollisionShape *TramShape;
  OgreBulletDynamics::RigidBody *TramrigidBody;
  OgreBulletCollisions::TriangleMeshCollisionShape *trackTrimesh;


  Ogre::SceneNode* nodeflecha;
  Ogre::Entity* flecha;
  Ogre::Vector3 vel;

  Ogre::Real force;
  Ogre::Vector3 posobjetomov;
  Ogre::Vector3 auxcamera1;
  Ogre::Vector3 auxcamera2;
  Ogre::Quaternion ori;
  OgreBulletDynamics::RigidBody *objetomov;


  Ogre::SceneNode *node_rock1;
  Ogre::SceneNode *node_boton;
  Ogre::SceneNode *node_globo;
  Ogre::SceneNode *node_cuerda;
  Ogre::SceneNode* node_entrada;
  Ogre::SceneNode* nodeyisus;
  Ogre::SceneNode *node_rock2;
  Ogre::SceneNode *node_rock3;

  int nflechas;
  bool _barrilpiedra;

  bool _exitGame;

 private:

  OIS::MouseState _mouse_position;
  bool flechaready;
};

#endif
