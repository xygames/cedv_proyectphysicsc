#include "PlayState.h"
#include "PauseState.h"

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "Shapes/OgreBulletCollisionsCylinderShape.h"
#include "OgreBulletCollisionsRay.h"
#include "OgreMath.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(33.98,1.2,-3.7));
  _camera->lookAt(Ogre::Vector3(0,0,0));
  //_camera->setDirection(0,-1,0);
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  // Nuevo background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
  // Creacion del modulo de debug visual de Bullet ------------------
  _debugDrawer = new OgreBulletCollisions::DebugDrawer();
  _debugDrawer->setDrawWireframe(true);	 
  Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->
    createChildSceneNode("debugNode", Ogre::Vector3::ZERO);
  node->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer));

  // Creacion del mundo (definicion de los limites y la gravedad) ---
  Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
    Ogre::Vector3 (-10000, -10000, -10000), 
    Ogre::Vector3 (-10000, -10000, -10000));
  Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,
 	   worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);
  _world->setShowDebugShapes (false);
  auxcamera1=Ogre::Vector3 (7,0,0);

///Sombras

  _sceneMgr->setAmbientLight(Ogre::ColourValue(0, 0, 0));
  _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

/////////////LUZ//////////////////////

  Ogre::Light* spotLight = _sceneMgr->createLight("SpotLight");
  spotLight->setType(Ogre::Light::LT_POINT);
  spotLight->setDirection(0, -1, -1);
  spotLight->setPosition(Ogre::Vector3(100, 100, 0));

  nflechas = 2;
  createscene();
  flechaready=true;
  _exitGame = false;
  _barrilpiedra=true;
}

void
PlayState::exit ()
{
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void
PlayState::pause()
{
}

void
PlayState::resume()
{
  // Se restaura el background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
  Ogre::Real deltaT = evt.timeSinceLastFrame;

  _world->stepSimulation(deltaT, 1); // Actualizar simulacion Bullet
/*
  if(!Rock2rigidBody->getBulletRigidBody()->checkCollideWithOverride(BotonrigidBody->getBulletRigidBody())){
  Abrotrampilla();
  }
*/

  posobjetomov = objetomov->getCenterOfMassPosition();

  Ogre::AxisAlignedBox aab = nodeflecha->_getWorldAABB().intersection(node_globo->_getWorldAABB());
  if(aab.isNull()){
    //std::cout<< "No corta"<<std::endl;  
  }
  else{
    std::cout<< "SI CORTA"<<std::endl;  
    Explotoglobo();
  }
  aab = nodeflecha->_getWorldAABB().intersection(node_entrada->_getWorldAABB());
  if(!aab.isNull()){
    ClavarFlecha(); 
  }
  Ogre::AxisAlignedBox aab2 = _sceneMgr->getSceneNode("Barril")->_getWorldAABB().intersection(node_boton->_getWorldAABB());
  if(!aab2.isNull()){
  Abrotrampilla();
  }

  aab=_sceneMgr->getSceneNode("Barril")->_getWorldAABB().intersection(node_rock2->_getWorldAABB());
 if(!aab.isNull() && _barrilpiedra){
BarrilrigidBody->setShape(_sceneMgr->getSceneNode("Barril"),BarrilShape,0,1,1.5,Ogre::Vector3(22.9,8.35,-2.5),Ogre::Quaternion::IDENTITY );
  Rock3rigidBody->setShape(node_rock3,Rock2Shape,0,1,50000,Ogre::Vector3(8.61,12,0),Ogre::Quaternion::IDENTITY ); 
Rock3rigidBody->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
  _barrilpiedra=false;
  objetomov=BarrilrigidBody;
  auxcamera1=auxcamera1 + Ogre::Vector3 (0,10,-10);
  }

  force+= deltaT;

   _camera->setPosition(posobjetomov + auxcamera1);
  _camera->lookAt(posobjetomov);

//DetectCollision();
  if (nflechas<0) lose();
  return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  Ogre::Real deltaT = evt.timeSinceLastFrame;

  _world->stepSimulation(deltaT, 1); // Actualizar simulacion Bullet
  if (_exitGame)
    return false;
  
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P) {
    pushState(PauseState::getSingletonPtr());
  }
  if (e.key == OIS::KC_A) {
    Abrotrampilla();
  }
  if (e.key == OIS::KC_R) {
    RecargoFlecha();
    nflechas--;
  }
  if (e.key == OIS::KC_Z) {
    Explotoglobo();
  }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_ESCAPE) {
 // Eliminar cuerpos rigidos --------------------------------------

  //delete ShapePlane,rigidBodyPlane,Rock1Shape,Rock1rigidBody,BaseShape,BaserigidBody;

  // Eliminar mundo dinamico y debugDrawer -------------------------
  delete _world->getDebugDrawer();    _world->setDebugDrawer(0);
  delete _world;
    _exitGame = true;

    
  }
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
 //_mouse_position=e.state;
  Ogre::Vector3 vt(0,0,0);     
  Ogre::Real deltaT = 0.02;
  btVector3 pos = btVector3(60,5.2,0); 

if (flechaready){

   btQuaternion orientacion = rigidflecha->getBulletRigidBody()->getOrientation();
   orientacion= orientacion * btQuaternion(0,0,(_mouse_position.X.rel + e.state.X.rel*deltaT)/5);



   btTransform trans = btTransform(orientacion,pos);
   rigidflecha->getBulletRigidBody()->proceedToTransform(trans);
   orientacion= orientacion * btQuaternion(0,(-_mouse_position.Y.rel - e.state.Y.rel*deltaT)/5,0);

   trans = btTransform(orientacion,pos);
   rigidflecha->getBulletRigidBody()->proceedToTransform(trans);

   ori=Ogre::Quaternion (static_cast<Ogre::Radian>(-e.state.X.rel*deltaT)/5,Ogre::Vector3 (0,1,0));
   vel=ori*vel;
   auxcamera1=ori*auxcamera1;
   
   //auxcamera2+=Ogre::Vector3 (-(-_mouse_position.X.rel - e.state.X.rel*deltaT)*3,0,0);
   ori=Ogre::Quaternion (static_cast<Ogre::Radian>(-e.state.Y.rel*deltaT)/5,Ogre::Vector3 (0,0,1));
   vel=ori*vel;
   auxcamera1=ori*auxcamera1;
   //auxcamera2+=Ogre::Vector3 (0,(-_mouse_position.Y.rel - e.state.Y.rel*deltaT)*3,0);


}
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  if(id==OIS::MB_Middle){
    
    _mouse_position = e.state;
  }
  if(id==OIS::MB_Left){
    force = 0;


  }
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{

     
  if(id==OIS::MB_Left && flechaready){
    //vel=Ogre::Vector3(-vel.x,vel.y,vel.z);
    if(force>0.9)
      force=0.9;
    std::cout<<force<<std::endl;
    rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,-9.8,0));
    rigidflecha->setLinearVelocity(vel*force*6+Ogre::Vector3 (0,0,10));
    rigidflecha->getBulletRigidBody()->setAngularVelocity(btVector3(0.5,0,0));
    auxcamera1=auxcamera1*2 + Ogre::Vector3 (0,10,0);
    flechaready=false;
  }
}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void PlayState::createscene(){
//PLANO
  // Creacion de la entidad y del SceneNode ------------------------
  Ogre::Plane plane1(Ogre::Vector3(0,1,0), 0);    // Normal y distancia
  Ogre::MeshManager::getSingleton().createPlane("p1",
	Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1,
	200, 200, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
  Ogre::SceneNode* node = _sceneMgr->createSceneNode("ground");
  Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt", "p1");
  groundEnt->setMaterialName("groundverde");
  node->attachObject(groundEnt);
  _sceneMgr->getRootSceneNode()->addChild(node);
  // Creamos forma de colision para el plano ----------------------- 

  ShapePlane = new OgreBulletCollisions::StaticPlaneCollisionShape
    (Ogre::Vector3(0,1,0), 0);   // Vector normal y distancia
  rigidBodyPlane = new OgreBulletDynamics::RigidBody("rigidBodyPlane", _world);
  // Creamos la forma estatica (forma, Restitucion, Friccion) ------
  rigidBodyPlane->setStaticShape(ShapePlane, 0.1, 0.8); 
  
  // Anadimos los objetos Shape y RigidBody ------------------------





//PEDRÁ 1
  Ogre::Vector3 size = Ogre::Vector3::ZERO;	
  Ogre::Vector3 position=Ogre::Vector3::ZERO;
  Ogre::Entity *rock1 = _sceneMgr->createEntity("Roca_12.mesh");
  node_rock1 = _sceneMgr->createSceneNode("Nodo_Piedra");
  node_rock1->attachObject(rock1);
  _sceneMgr->getRootSceneNode()->addChild(node_rock1);
  node_rock1->setPosition(Ogre::Vector3(23.18,10.55,-11.17));
  //node_rock1->showBoundingBox(true);


// Fisica--------------------------------------------
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter3 = new 
    OgreBulletCollisions::StaticMeshToShapeConverter(rock1);
  Rock1Shape = trimeshConverter3->createConvex();
  delete trimeshConverter3;

  Rock1rigidBody = new OgreBulletDynamics::RigidBody("Rock1rigidBody", _world);


 // Rock1rigidBody->getBulletRigidBody()->setGravity(btVector3 (0,0,0));




//BASE BOTON
  Ogre::Entity *base = _sceneMgr->createEntity("Base","Base_boton2.mesh");
  Ogre::SceneNode *node_base = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node_base->attachObject(base);
  //node_base->setScale(2,1,2);

// Fisica-------------------
    Ogre::AxisAlignedBox boundingB = base->getBoundingBox();
    size = boundingB.getSize(); 
    size /= 2.0f;   // El tamano en Bullet se indica desde el centro
    BaseShape = new OgreBulletCollisions::BoxCollisionShape(size);
    BaserigidBody = new OgreBulletDynamics::RigidBody("BaserigidBody",_world);

  BaserigidBody->setShape(node_base, BaseShape,
		     0.6 /* Restitucion */, 0.6 /* Friccion */,
		     0 /* Masa */, Ogre::Vector3(23,0.31,11.37) /* Posicion inicial */,
		     Ogre::Quaternion::IDENTITY /* Orientacion */);

//BOTON
  Ogre::Entity *boton = _sceneMgr->createEntity("Boton2.mesh");
  node_boton = _sceneMgr->createSceneNode("Nodo_Boton");
  node_boton->attachObject(boton);
  _sceneMgr->getRootSceneNode()->addChild(node_boton);
 // node_boton->setScale(1,1,1);
  //node_boton->showBoundingBox(true); 	

// Fisica-------------------
    boundingB = boton->getBoundingBox();
    size = boundingB.getSize(); 
    size /= 2.0f;   // El tamano en Bullet se indica desde el centro
    BotonShape = new OgreBulletCollisions::BoxCollisionShape(size);

  BotonrigidBody = new OgreBulletDynamics::RigidBody("BotonrigidBody", _world);

  BotonrigidBody->setShape(node_boton, BotonShape,0.6,0.6,100,Ogre::Vector3(23,0.75,11.37),Ogre::Quaternion::IDENTITY );

//GLOBO
  Ogre::Entity *globo = _sceneMgr->createEntity("Globo","Globo2.mesh");
  node_globo = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node_globo->attachObject(globo);
  node_globo->setPosition(Ogre::Vector3 (23.18,15.38,-11.17));
//CUERDA
  Ogre::Entity *cuerda = _sceneMgr->createEntity("Cuerda","Cuerda2.mesh");
  node_cuerda = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node_cuerda->attachObject(cuerda);
  node_cuerda->setPosition(Ogre::Vector3 (23.18,15.38,-11.17));
  node_cuerda->pitch(Ogre::Degree(90));

//PEDRÁ 2

  Ogre::Entity *rock2 = _sceneMgr->createEntity("Piedra2","Roca_22.mesh");
  node_rock2 = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node_rock2->attachObject(rock2);
  node_rock2->setPosition(Ogre::Vector3(23.5,0.2,-3.7));


// Fisica--------------------------------------------
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = new 
  OgreBulletCollisions::StaticMeshToShapeConverter(rock2);
  Rock2Shape = trimeshConverter->createConvex();
  delete trimeshConverter;

  Rock2rigidBody = new OgreBulletDynamics::RigidBody("Rock2rigidBody", _world);




//PEDRÁ 3

  Ogre::Entity *rock3 = _sceneMgr->createEntity("Piedra3","Roca_3.mesh");
  node_rock3 = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node_rock3->attachObject(rock3);
  //node_rock2->setScale(0.4,0.4,0.4);
  node_rock3->setPosition(Ogre::Vector3(8.61,10.8,0));

// Fisica--------------------------------------------
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter8 = new 
  OgreBulletCollisions::StaticMeshToShapeConverter(rock3);
  Rock3Shape = trimeshConverter8->createConvex();
  delete trimeshConverter8;

  Rock3rigidBody = new OgreBulletDynamics::RigidBody("Rock3rigidBody", _world);
/*
  Rock3rigidBody->setShape(node_rock3,Rock2Shape,0.6,0.6,5,Ogre::Vector3(8.61,12,0),Ogre::Quaternion::IDENTITY ); 
Rock3rigidBody->getBulletRigidBody()->setGravity(btVector3 (0,0,0)); 
*/
//ENTRADA
  node_entrada = _sceneMgr->createSceneNode("Entrada");
  Ogre::Entity* entrada = _sceneMgr->createEntity("Entrada2.mesh");
  node_entrada->attachObject(entrada);
  _sceneMgr->getRootSceneNode()->addChild(node_entrada);
  node_entrada->setScale(1.5,1.5,1.5);
  entrada->setCastShadows(false);
//Fisica---------------------------------
OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter4 = new 
    OgreBulletCollisions::StaticMeshToShapeConverter(entrada);

  trackTrimesh = trimeshConverter4->createTrimesh();

  EntradarigidBody = new 
    OgreBulletDynamics::RigidBody("track", _world);
  EntradarigidBody->setShape(node_entrada, trackTrimesh, 0.8, 0.95, 0, Ogre::Vector3(0,0,0),Ogre::Quaternion::IDENTITY);

  delete trimeshConverter4;
//Trampilla
  Ogre::SceneNode* node_trampilla = _sceneMgr->createSceneNode("Trampilla");
  Ogre::Entity* trampilla = _sceneMgr->createEntity("Trampilla2.mesh");
  node_trampilla->attachObject(trampilla);
  _sceneMgr->getRootSceneNode()->addChild(node_trampilla);
  node_trampilla->setPosition(Ogre::Vector3(7.76,9.72,0.05));

//FLECHA

  flecha = _sceneMgr->createEntity("arrow.mesh");
  nodeflecha = _sceneMgr->createSceneNode("Flecha");
  nodeflecha->attachObject(flecha);
  //nodeflecha->setScale(50,50,50);
  nodeflecha->setPosition(Ogre::Vector3(0,5,-42));
  _sceneMgr->getRootSceneNode()->addChild(nodeflecha);

//fisica
  OgreBulletCollisions::CylinderCollisionShape *flechaShape = 
    new OgreBulletCollisions::CylinderCollisionShape(Ogre::Vector3 (1.5,0.35,0), Ogre::Vector3(0,0,1)); 
  rigidflecha = new OgreBulletDynamics::RigidBody("flechaRigid", _world);
  rigidflecha->setShape(nodeflecha, flechaShape, 0, 0.6, 0.1,Ogre::Vector3(0,0,0), Ogre::Quaternion::IDENTITY);
  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
  rigidflecha->disableDeactivation();
  
  btQuaternion orientacion = btQuaternion (-SIMD_2_PI*0.25,SIMD_2_PI*0.25,0); 
  btVector3 pos = btVector3(60,5.2,0);

  btTransform trans = btTransform(orientacion,pos);
  rigidflecha->getBulletRigidBody()->proceedToTransform(trans);

  vel = Ogre::Vector3 (-10,0,0);

  objetomov = rigidflecha;

//ENEMY
  Ogre::Entity* yisus = _sceneMgr->createEntity("jesus-base.mesh");
  nodeyisus = _sceneMgr->createSceneNode("Yisus");
  Ogre::Entity* tunica = _sceneMgr->createEntity("Tunica.mesh");
  Ogre::SceneNode* nodetunica = _sceneMgr->createSceneNode("Tunica");
  nodeyisus->attachObject(yisus);
  nodetunica->attachObject(tunica);
  nodeyisus->addChild(nodetunica);
  //nodeflecha->setScale(50,50,50);
  nodeyisus->setPosition(Ogre::Vector3(0,5,-42));
  _sceneMgr->getRootSceneNode()->addChild(nodeyisus);
  nodeyisus->setPosition(Ogre::Vector3(9,0,0));
  nodeyisus->pitch(Ogre::Degree(90));
  nodeyisus->roll(Ogre::Degree(-90));
  nodeyisus->setScale(3,3,3);

//RAMPA
  Ogre::Entity* rampa = _sceneMgr->createEntity("Rampa.mesh");
  Ogre::SceneNode* noderampa = _sceneMgr->createSceneNode("Rampa");
  noderampa->attachObject(rampa);
  _sceneMgr->getRootSceneNode()->addChild(noderampa);
//fisicas
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter5 = new 
  OgreBulletCollisions::StaticMeshToShapeConverter(rampa);
  RampaShape = trimeshConverter5->createConvex();
  delete trimeshConverter5;

  RamparigidBody = new OgreBulletDynamics::RigidBody("RamparigidBody", _world);

  RamparigidBody->setShape(noderampa,   RampaShape,0.6,0.6,0,Ogre::Vector3(23.08,4.8,2.15),Ogre::Quaternion::IDENTITY );

//BARRIL
  Ogre::Entity* barril = _sceneMgr->createEntity("Barril.mesh");
  Ogre::SceneNode* nodebarril = _sceneMgr->createSceneNode("Barril");
  nodebarril->attachObject(barril);
  _sceneMgr->getRootSceneNode()->addChild(nodebarril);
  nodebarril->setPosition(Ogre::Vector3(22.9,8.35,-2.5));
//nodebarril->showBoundingBox(true); 
//fisicas
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter2 = new 
  OgreBulletCollisions::StaticMeshToShapeConverter(barril);
  BarrilShape = trimeshConverter2->createConvex();
  delete trimeshConverter2;

  BarrilrigidBody = new OgreBulletDynamics::RigidBody("BarrilrigidBody", _world);

  //BarrilrigidBody->setShape(nodebarril,BarrilShape,0,4100000,500000,Ogre::Vector3(22.8,8.35,-2.3),Ogre::Quaternion::IDENTITY );
  //BarrilrigidBody->getBulletRigidBody()->setGravity(btVector3 (0,0,0));

//BASETRAMP
  Ogre::Entity* basetram = _sceneMgr->createEntity("Base_trampolin.mesh");
  Ogre::SceneNode* nodebasetram = _sceneMgr->createSceneNode("Basetram");
  nodebasetram->attachObject(basetram);
  _sceneMgr->getRootSceneNode()->addChild(nodebasetram);
//fisicas
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter6 = new 
  OgreBulletCollisions::StaticMeshToShapeConverter(basetram);
  BaseTramShape = trimeshConverter6->createConvex();
  delete trimeshConverter6;

  BaseTramrigidBody = new OgreBulletDynamics::RigidBody("BaseTramrigidBody", _world);

  BaseTramrigidBody->setShape(nodebasetram,   BaseTramShape,0.6,0.6,0,Ogre::Vector3(23.27,0,-7),Ogre::Quaternion::IDENTITY );
//TRAMP
  Ogre::Entity* tram = _sceneMgr->createEntity("Tabla_trampolin.mesh");
  Ogre::SceneNode* nodetram = _sceneMgr->createSceneNode("Tram");
  nodetram->attachObject(tram);
  _sceneMgr->getRootSceneNode()->addChild(nodetram);
  nodetram->setPosition(Ogre::Vector3(23.27,0.5,-6.5));
  nodetram->pitch(Ogre::Degree(20));
//fisicas
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter7 = new 
  OgreBulletCollisions::StaticMeshToShapeConverter(tram);
  TramShape = trimeshConverter7->createConvex();
  delete trimeshConverter7;

  TramrigidBody = new OgreBulletDynamics::RigidBody("TramrigidBody", _world);




}

void PlayState::Abrotrampilla(){
  _sceneMgr->getSceneNode("Trampilla")->detachAllObjects();
  Rock3rigidBody->getBulletRigidBody()->setGravity(btVector3 (0,-9.8,0));
  objetomov = Rock3rigidBody;
}




void PlayState::Explotoglobo(){
  _sceneMgr->getEntity("Globo")->getParentSceneNode()->detachAllObjects();
  _sceneMgr->getEntity("Cuerda")->getParentSceneNode()->detachAllObjects();
  //_sceneMgr->getEntity("Cuerda");
  Rock1rigidBody->setShape(node_rock1, Rock1Shape,0,0.5,500,Ogre::Vector3(23.18,10.55,-11.17),Ogre::Quaternion::IDENTITY );
  Rock2rigidBody->setShape(node_rock2,   Rock2Shape,0.6,0.6,0.5,Ogre::Vector3(23.5,2,-3.7),Ogre::Quaternion::IDENTITY );
  TramrigidBody->setShape(_sceneMgr->getSceneNode("Tram"),TramShape,0.6,0.6,1,Ogre::Vector3(23.27,0.5,-7),Ogre::Quaternion::IDENTITY );
  objetomov=Rock1rigidBody;
  auxcamera1=auxcamera1*2 + Ogre::Vector3 (0,5,0);
}



void PlayState::RecargoFlecha() {
  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
  rigidflecha->disableDeactivation();

  btQuaternion orientacion = btQuaternion (-SIMD_2_PI*0.25,SIMD_2_PI*0.25,0); 
  btVector3 pos = btVector3(60,5.2,0);


  btTransform trans = btTransform(orientacion,pos);
  rigidflecha->getBulletRigidBody()->proceedToTransform(trans);

   vel = Ogre::Vector3 (-10,0,0);
    rigidflecha->setLinearVelocity(Ogre::Vector3 (0,0,0));
  rigidflecha->getBulletRigidBody()->setAngularVelocity(btVector3(0,0,0));
  objetomov=rigidflecha;
  auxcamera1=Ogre::Vector3 (7,0,0);
  flechaready=true;
}

void PlayState::ClavarFlecha(){
   vel = Ogre::Vector3 (-10,0,0);
  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
    rigidflecha->setLinearVelocity(Ogre::Vector3 (0,0,0));
  rigidflecha->getBulletRigidBody()->setAngularVelocity(btVector3(0,0,0));
}

void PlayState::lose(){
  flechaready=false;
  std::cout << "HAS PERDIDO"<<std::endl;


}
