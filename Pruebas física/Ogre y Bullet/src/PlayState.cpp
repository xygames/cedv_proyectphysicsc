#include "PlayState.h"
#include "PauseState.h"
#include "MyMotionState.h"

#include <OIS/OIS.h>






template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(0,50,50));
  _camera->lookAt(Ogre::Vector3(0,-3,-1));

  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  // Nuevo background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

////////////

 _broadphase = new btDbvtBroadphase();
  _collisionConf = new btDefaultCollisionConfiguration();
  _dispatcher = new btCollisionDispatcher(_collisionConf);
  _solver = new btSequentialImpulseConstraintSolver;
  _world = new btDiscreteDynamicsWorld(_dispatcher,_broadphase,
				       _solver,_collisionConf);
  
  // Establecimiento propiedades del mundo
  _world->setGravity(btVector3(0,-10,0));

  // Creacion de los elementos iniciales del mundo




  createScene();
  _exitGame = false;
}


void PlayState::createScene() {


  //-------------------------Suelo----------------------------

  Ogre::Plane plane1(Ogre::Vector3(0,1,0), 1);  // Normal y distancia
  Ogre::MeshManager::getSingleton().createPlane("plane1", 		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 		plane1,200,100,1,1,true,1,20,20,Ogre::Vector3::UNIT_Z);
  Ogre::SceneNode* node2 = _sceneMgr->createSceneNode("ground");
  Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt",
	"plane1");

  groundEnt->setMaterialName("ground");
  node2->attachObject(groundEnt);
  _sceneMgr->getRootSceneNode()->addChild(node2);



  // Creamos las formas de colision --------------------------------- 
  _groundShape = new btStaticPlaneShape(btVector3(0,1,0),1);

  // Creamos el plano -----------------------------------------------
  MyMotionState* groundMotionState = new MyMotionState(
    btTransform(btQuaternion(0,0,0,1),btVector3(0,0,0)), node2);
  btRigidBody::btRigidBodyConstructionInfo
    groundRigidBodyCI(0,groundMotionState,_groundShape,btVector3(0,0,0));
  _groundRigidBody = new btRigidBody(groundRigidBodyCI);
  _world->addRigidBody(_groundRigidBody);



  //------------------------Bola--------------------------------
  /////////Ball creation

  _ballNode = _sceneMgr->createSceneNode("bola");
  Ogre::Entity* ball = _sceneMgr->createEntity("ball.mesh");
  ball->setMaterialName("ball");
  ball->setCastShadows(true);
  //_ballNode->scale(1,1,1);
  _ballNode->attachObject(ball);
  Ogre::Vector3 vball(0,0,0);
  _ballNode->setPosition(vball);
  _sceneMgr->getRootSceneNode()->addChild(_ballNode);

 
  // Creamos las formas de colision --------------------------------- 
  _ballShape = new btSphereShape(1);

  MyMotionState* ballMotionState = new MyMotionState(
     btTransform(btQuaternion(0,0,0,1),btVector3(0,50,0)), _ballNode);
  
  btScalar mass = 1;
  btVector3 ballInertia(0,0,0);
  _ballShape->calculateLocalInertia(mass,ballInertia);

  btRigidBody::btRigidBodyConstructionInfo ballRigidBodyCI(
     mass,ballMotionState,_ballShape,ballInertia);
  _ballRigidBody = new btRigidBody(ballRigidBodyCI);
  _world->addRigidBody(_ballRigidBody);





/*

  //---------------------Ladrillos------------------------------

  ladrillo = _sceneMgr->createEntity("cubo.mesh");
  nodeladrillo = _sceneMgr->createSceneNode("ladrilloNode");
  nodeladrillo->attachObject(ladrillo);
  _sceneMgr->getRootSceneNode()->addChild(nodeladrillo);
  nodeladrillo->setPosition(0,100000,0);

*/

}



void
PlayState::exit ()
{
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();


  _world->removeRigidBody(_ballRigidBody);
  delete _ballRigidBody->getMotionState();  delete _ballRigidBody;
  
  _world->removeRigidBody(_groundRigidBody);
  delete _groundRigidBody->getMotionState();  delete _groundRigidBody;
  
  delete _ballShape;      delete _groundShape;
  
  delete _world;  delete _solver;
  delete _collisionConf;
  delete _dispatcher;     delete _broadphase;


}

void
PlayState::pause()
{
}

void
PlayState::resume()
{
  // Se restaura el background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
  Ogre::Vector3 vt(0,0,0);     Ogre::Real tSpeed = 20.0;  

  Ogre::Real deltaT = evt.timeSinceLastFrame;
  int fps = 1.0 / deltaT;

  _world->stepSimulation(deltaT, 1); // Actualizar simulacion Bullet



  return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P) {
    pushState(PauseState::getSingletonPtr());
  }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
  }
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
