/*********************************************************************
 * Módulo 2. Curso de Experto en Desarrollo de Videojuegos
 * Autor: Carlos González Morcillo     Carlos.Gonzalez@uclm.es
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/
#include "MyFrameListener.h"

#include "Shapes/OgreBulletCollisionsTrimeshShape.h"	
#include "Shapes/OgreBulletCollisionsSphereShape.h"	
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"

MyFrameListener::MyFrameListener(RenderWindow* win, 
				 Camera* cam, 
				 OverlayManager *om, 
				 SceneManager *sm) {
  OIS::ParamList param;
  size_t windowHandle;  std::ostringstream wHandleStr;

  _camera = cam;  _overlayManager = om; _sceneManager = sm;
  
  win->getCustomAttribute("WINDOW", &windowHandle);
  wHandleStr << windowHandle;
  param.insert(std::make_pair("WINDOW", wHandleStr.str()));
  
  _inputManager = OIS::InputManager::createInputSystem(param);
  _keyboard = static_cast<OIS::Keyboard*>
    (_inputManager->createInputObject(OIS::OISKeyboard, false));
  _mouse = static_cast<OIS::Mouse*>
    (_inputManager->createInputObject(OIS::OISMouse, false));
  _mouse->getMouseState().width = win->getWidth();
  _mouse->getMouseState().height = win->getHeight();

  _numEntities = 0;    // Numero de Shapes instanciadas
  _timeLastObject = 0; // Tiempo desde que se añadio el ultimo objeto

  // Creacion del modulo de debug visual de Bullet ------------------
  _debugDrawer = new OgreBulletCollisions::DebugDrawer();
  _debugDrawer->setDrawWireframe(true);	 
  SceneNode *node = _sceneManager->getRootSceneNode()->
    createChildSceneNode("debugNode", Vector3::ZERO);
  node->attachObject(static_cast <SimpleRenderable *>(_debugDrawer));

  // Creacion del mundo (definicion de los limites y la gravedad) ---
  AxisAlignedBox worldBounds = AxisAlignedBox (
    Vector3 (-100, -100, -100), 
    Vector3 (100,  100,  100));
  Vector3 gravity = Vector3(0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld(_sceneManager,
 	   worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);

  // Creacion de los elementos iniciales del mundo
  CreateInitialWorld();
}

MyFrameListener::~MyFrameListener() {
  _inputManager->destroyInputObject(_keyboard);
  _inputManager->destroyInputObject(_mouse);
  OIS::InputManager::destroyInputSystem(_inputManager);

  // Eliminar cuerpos rigidos --------------------------------------
  std::deque <OgreBulletDynamics::RigidBody *>::iterator 
     itBody = _bodies.begin();
  while (_bodies.end() != itBody) {   
    delete *itBody;  ++itBody;
  } 
 
  // Eliminar formas de colision -----------------------------------
  std::deque<OgreBulletCollisions::CollisionShape *>::iterator 
    itShape = _shapes.begin();
  while (_shapes.end() != itShape) {   
    delete *itShape; ++itShape;
  } 

  _bodies.clear();  _shapes.clear();

  // Eliminar mundo dinamico y debugDrawer -------------------------
  delete _world->getDebugDrawer();    _world->setDebugDrawer(0);
  delete _world;
}

void MyFrameListener::CreateInitialWorld() {
  // Creacion del track --------------------------------------------------
  Entity *entity = _sceneManager->createEntity("track.mesh");
  SceneNode *node = _sceneManager->createSceneNode("track");
  node->attachObject(entity);

  _sceneManager->getRootSceneNode()->addChild(node);
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = new 
    OgreBulletCollisions::StaticMeshToShapeConverter(entity);

  OgreBulletCollisions::TriangleMeshCollisionShape *trackTrimesh = 
    trimeshConverter->createTrimesh();

  OgreBulletDynamics::RigidBody *rigidTrack = new 
    OgreBulletDynamics::RigidBody("track", _world);
  rigidTrack->setShape(node, trackTrimesh, 0.8, 0.95, 0, Vector3::ZERO, 
		       Quaternion::IDENTITY);

  delete trimeshConverter;

  // Creacion del sumidero  ---------------------------------------------
  Entity *entity2 = _sceneManager->createEntity("drain.mesh");
  SceneNode *node2 = _sceneManager->createSceneNode("drain");
  node2->attachObject(entity2);

  _sceneManager->getRootSceneNode()->addChild(node2);
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter2 = new 
    OgreBulletCollisions::StaticMeshToShapeConverter(entity2);

  OgreBulletCollisions::TriangleMeshCollisionShape *Trimesh2 = 
    trimeshConverter2->createTrimesh();

  OgreBulletDynamics::RigidBody *rigidObject2 = new 
    OgreBulletDynamics::RigidBody("drain", _world);
  rigidObject2->setShape(node2, Trimesh2, 0.5, 0.5, 0, Vector3::ZERO, 
			 Quaternion::IDENTITY);
}

void MyFrameListener::AddDynamicObject() {
  _timeLastObject = 0.15;   // Segundos para anadir uno nuevo... 

  Vector3 size = Vector3::ZERO;
  Vector3 pos = Vector3::ZERO;
  
  if (_keyboard->isKeyDown(OIS::KC_B)) pos = Vector3(-0.14, 1.07, -0.08); 
  if (_keyboard->isKeyDown(OIS::KC_V)) pos = Vector3(0.85, 1.07, -0.08);
   
  Entity *entity = _sceneManager->createEntity("ball" + 
     StringConverter::toString(_numEntities), "ball.mesh");
  SceneNode *node = _sceneManager->getRootSceneNode()->
    createChildSceneNode();
  node->attachObject(entity);

  OgreBulletCollisions::SphereCollisionShape *ballShape = 
    new OgreBulletCollisions::SphereCollisionShape(0.02);

  OgreBulletDynamics::RigidBody *rigidBall = new 
    OgreBulletDynamics::RigidBody("ball" + 
      StringConverter::toString(_numEntities), _world);
  rigidBall->setShape(node, ballShape, 0.05, 0.05, 0.3, pos, 
		      Quaternion::IDENTITY);
  _numEntities++;

  // Anadimos los objetos a las deques
  _shapes.push_back(ballShape);   _bodies.push_back(rigidBall);
}

bool MyFrameListener::frameStarted(const Ogre::FrameEvent& evt) {
  Ogre::Vector3 vt(0,0,0);     Ogre::Real tSpeed = 20.0;  
  Ogre::Real deltaT = evt.timeSinceLastFrame;
  int fps = 1.0 / deltaT;
  bool mbleft, mbmiddle, mbright; // Botones del raton pulsados

  _world->stepSimulation(deltaT); // Actualizar simulacion Bullet
  _timeLastObject -= deltaT;

  _keyboard->capture();
  if (_keyboard->isKeyDown(OIS::KC_ESCAPE)) return false;
  if ((_keyboard->isKeyDown(OIS::KC_B) || _keyboard->isKeyDown(OIS::KC_V))
     && (_timeLastObject <= 0)) AddDynamicObject();
  
  if (_keyboard->isKeyDown(OIS::KC_D)) _world->setShowDebugShapes (true); 
  if (_keyboard->isKeyDown(OIS::KC_H)) _world->setShowDebugShapes (false); 


  int posx = _mouse->getMouseState().X.abs;   // Posicion del puntero
  int posy = _mouse->getMouseState().Y.abs;   //  en pixeles.

  _camera->moveRelative(vt * deltaT * tSpeed);
  if (_camera->getPosition().length() < 10.0) {
    _camera->moveRelative(-vt * deltaT * tSpeed);
  }

  _mouse->capture();

 // Si usamos la rueda, desplazamos en Z la camara ------------------
  vt+= Ogre::Vector3(0,0,-0.5)*deltaT * _mouse->getMouseState().Z.rel;   
  _camera->moveRelative(vt * deltaT * tSpeed);

  // Botones del raton pulsados? -------------------------------------
  mbleft = _mouse->getMouseState().buttonDown(OIS::MB_Left);
  mbmiddle = _mouse->getMouseState().buttonDown(OIS::MB_Middle);
  mbright = _mouse->getMouseState().buttonDown(OIS::MB_Right);

  if (mbmiddle) { // Con boton medio pulsado, rotamos camara ---------
    float rotx = _mouse->getMouseState().X.rel * deltaT * -1;
    float roty = _mouse->getMouseState().Y.rel * deltaT * -1;
    _camera->yaw(Ogre::Radian(rotx));
    _camera->pitch(Ogre::Radian(roty));
  }

  Ogre::OverlayElement *oe;
  oe = _overlayManager->getOverlayElement("cursor");
  oe->setLeft(posx);  oe->setTop(posy);

  oe = _overlayManager->getOverlayElement("fpsInfo");
  oe->setCaption(Ogre::StringConverter::toString(fps));

  oe = _overlayManager->getOverlayElement("nEntitiesInfo");
  oe->setCaption(Ogre::StringConverter::toString(_numEntities));

  return true;
}

bool MyFrameListener::frameEnded(const Ogre::FrameEvent& evt) {
  Real deltaT = evt.timeSinceLastFrame;
  _world->stepSimulation(deltaT); // Actualizar simulacion Bullet
  return true;
}
