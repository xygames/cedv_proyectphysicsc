#include "PlayState.h"
#include "PauseState.h"
#include <OgreStringConverter.h>
#include <cstdlib>
#include <iostream>
#include <ctime>


template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{

  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(0,30,30));
  _camera->lookAt(Ogre::Vector3(0,-1,-1));

  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);

  _node = _sceneMgr->getRootSceneNode()->createChildSceneNode("P");
    



  _viewport = _root->getAutoCreatedWindow()->getViewport(0);
  // Nuevo background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

  // Crear info.
  _overlayManager = Ogre::OverlayManager::getSingletonPtr();
  _overlay = _overlayManager->getByName("Info");
  _overlay->show();

  // Ocultar logo Game Over
  oe = _overlayManager->getOverlayElement("logoGO");
  oe->hide();

  // Ocultar overlay Fase Superada
  oe = _overlayManager->getOverlayElement("logoClear");
  oe->hide();

  oe = _overlayManager->getOverlayElement("instrucciones");

  oe->show();

  _last_time = _time_count = 0;  

  createScene();
  _raySceneQuery = _sceneMgr->createRayQuery(Ogre::Ray());  

  _exitGame = false;
  frame = 0;
  alpha =1.005;



  CEGUI::MouseCursor::getSingleton().hide( );
  puntuacion = 0;
  ladrillos = 0;
  gameReady = -1;

}


//CREANDO LA ESCENA
void PlayState::createScene() {
  vdx = Ogre::Vector3(0.59,0,0);
  vdz = Ogre::Vector3(0,0,-1);
  vdxpala = Ogre::Vector3(0,0,0);

  life=3;

  _ladri.reserve(10);
  _ladriM.reserve(5);





  //-------------------------Suelo----------------------------

  Ogre::Plane plane1(Ogre::Vector3::UNIT_Y, 1);
  Ogre::MeshManager::getSingleton().createPlane("plane1", 		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 		plane1,240,140,1,1,true,1,1,1,Ogre::Vector3::UNIT_Z);
  Ogre::SceneNode* node2 = _sceneMgr->createSceneNode("ground");
  Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt",
	"plane1");
  node2->setScale(1,1,1);
  groundEnt->setMaterialName("ground");
  node2->attachObject(groundEnt);
   _node->addChild(node2);


  
  //------------------------Bola--------------------------------
  ball = _sceneMgr->createEntity("ball.mesh");
  nodeball = _sceneMgr->createSceneNode("ballNode");
  nodeball->attachObject(ball);
  _node->addChild(nodeball);
  nodeball->translate(27,1,15);







  //------------------------Pala--------------------------------
  pala = _sceneMgr->createEntity("pala.mesh");
  nodepala = _sceneMgr->createSceneNode("palaNode");
  nodepala->attachObject(pala);
  _node->addChild(nodepala);
  nodepala->setScale(5,1,1);
  nodepala->translate(10,5,20);

  //---------------------Ladrillos------------------------------
  Ogre::SceneNode * _node3;
  std::stringstream ladrilloName;
  std::string s = "LADRILLO";
  for (int i=0;i < 5;i++){
    for (int j=0;j < 10;j++){
      //base
      ladrilloName << s << totalLadrillos;
      totalLadrillos = totalLadrillos+1;
      Ogre::Entity* ent1 = _sceneMgr->createEntity(ladrilloName.str(),"cubo.mesh");
      ent1->setQueryFlags(LADRILLO);
      _node3 = _sceneMgr->createSceneNode();
      _node3->attachObject(ent1);
      _node3->setPosition(-15 + j*5,5,-25 + i*3);;
      _node3->scale(2.5,1,1);
      _node->addChild(_node3);
      ladrilloName.str("");
      _ladri.push_back(_node3);
      //_nodes.push_back(_node3);
    }
  _ladriM.push_back(_ladri);
  _ladri.clear();
  } 


  mask = LADRILLO;

}


void
PlayState::exit ()
{
  std::cout << "EXIT PLAYSTATE"<<std::endl;
  //_s.str(" ");
  _overlay->hide();

  CEGUI::MouseCursor::getSingleton().show( );


  //_root->getAutoCreatedWindow()->removeAllViewports();
  //_sceneMgr->clearScene();
  _sceneMgr->destroyEntity("planeEnt");
  std::stringstream ladrilloName;
  std::string s = "LADRILLO";
  for (int i=0;i < totalLadrillos;i++){
    ladrilloName << s << i;
    _sceneMgr->destroyEntity(ladrilloName.str());
    ladrilloName.str("");
   }
  //_sceneMgr->destroySceneNode("ground");
  _sceneMgr->destroySceneNode("ballNode");
  _sceneMgr->destroySceneNode("palaNode");
  //_overlayManager->destroyAll();
  _node->removeAndDestroyAllChildren();
  _sceneMgr->destroySceneNode(_node);



  

}

void
PlayState::pause()
{
 _overlay->hide();
}

void
PlayState::resume()
{
  _overlay->show();

}
//////////////////////////////////////////////

/////////////////////////////////////////////
bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{

  //Time Control Overlay
  deltaT = evt.timeSinceLastFrame;
  oe = _overlayManager->getOverlayElement("timeinf");
  _last_time += deltaT;
  //_s.str("");
  //_s << std::setprecision(1)<<std::fixed << _last_time;
  oe->setCaption(Ogre::StringConverter::toString(_last_time));

  //Points Control Overlay
  
  puntuacion = ladrillos *100;
  oe = _overlayManager->getOverlayElement("minesinf");
  oe->setCaption(Ogre::StringConverter::toString(puntuacion));

  //Lifes Control Overlay

  oe = _overlayManager->getOverlayElement("fpsInfo");
  oe->setCaption(Ogre::StringConverter::toString(life));

  return true;

}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P) {
    pushState(PauseState::getSingletonPtr());
  }
   else{
 
    if(e.key == OIS::KC_LEFT){
    if(gameReady == -1){
        oe = _overlayManager->getOverlayElement("instrucciones");
  	oe->hide(); 
	gameReady = 1;
    }
    vdxpala = vdxpala + Ogre::Vector3(-1,0,0);
    }
    if(e.key == OIS::KC_RIGHT){
    if(gameReady == -1){
        oe = _overlayManager->getOverlayElement("instrucciones");
  	oe->hide(); 
	gameReady = 1;
    }
    vdxpala = vdxpala + Ogre::Vector3(1,0,0);
    }
}
  if (e.key == OIS::KC_ESCAPE) {
  gameReady=0;

  }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
    popState();
  }
  else if (e.key == OIS::KC_LEFT){
  vdxpala = vdxpala - Ogre::Vector3(-1,0,0);
  }
  else if(e.key == OIS::KC_RIGHT){
  vdxpala = vdxpala - Ogre::Vector3(1,0,0);
  } 


}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}






