#include "PlayState.h"
#include "PauseState.h"

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"	
#include "Shapes/OgreBulletCollisionsSphereShape.h"	
#include "Shapes/OgreBulletCollisionsCylinderShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"








template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(0,20,50));
  _camera->lookAt(Ogre::Vector3(0,0,-1));

  //_camera->setOrientation(Ogre::Quaternion(4,1,0,0));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  // Nuevo background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

////////////
   // Numero de Shapes instanciadas
  _timeLastObject = 0; // Tiempo desde que se añadio el ultimo objeto
 
  // Creacion del modulo de debug visual de Bullet ------------------
  _debugDrawer = new OgreBulletCollisions::DebugDrawer();
  _debugDrawer->setDrawWireframe(true);	 
  Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->
    createChildSceneNode("debugNode", Ogre::Vector3::ZERO);
  node->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer));

  // Creacion del mundo (definicion de los limites y la gravedad) ---
  Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
    Ogre::Vector3 (-100, -100, -100), 
    Ogre::Vector3 (100,  100,  100));
  Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,
 	   worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);

/////////////////OIS/////////////
  _key_pressed= false;

//////////////////////////////////////
  createScene();
  _exitGame = false;
}


void PlayState::createScene() {


  //-------------------------Suelo----------------------------

  Ogre::Plane plane1(Ogre::Vector3(0,1,0), 1);  // Normal y distancia
  Ogre::MeshManager::getSingleton().createPlane("plane1", 		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 		plane1,200,100,1,1,true,1,20,20,Ogre::Vector3::UNIT_Z);
  Ogre::SceneNode* node2 = _sceneMgr->createSceneNode("Plano");
  Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt",
	"plane1");

  groundEnt->setMaterialName("ground");
  node2->attachObject(groundEnt);
  _sceneMgr->getRootSceneNode()->addChild(node2);

// Creamos forma de colision para el plano ----------------------- 
  OgreBulletCollisions::CollisionShape *Shape;
  Shape = new OgreBulletCollisions::StaticPlaneCollisionShape
    (Ogre::Vector3(0,1,0),1);   // Vector normal y distancia
  OgreBulletDynamics::RigidBody *rigidBodyPlane = new 
    OgreBulletDynamics::RigidBody("Plano", _world);

  // Creamos la forma estatica (forma, Restitucion, Friccion) ------
  rigidBodyPlane->setStaticShape(Shape, 0.1, 0.8); 
  
  // Anadimos los objetos Shape y RigidBody ------------------------
  _shapes.push_back(Shape);      _bodies.push_back(rigidBodyPlane);





  //------------------------Bola--------------------------------
  /////////Ball creation

  _ballNode = _sceneMgr->createSceneNode("bola");
  Ogre::Entity* ball = _sceneMgr->createEntity("ball.mesh");
  ball->setMaterialName("ball");
  ball->setCastShadows(true);
  //_ballNode->scale(1,1,1);
  _ballNode->attachObject(ball);
  Ogre::Vector3 vball(0,0,0);
  _ballNode->setPosition(vball);
  _sceneMgr->getRootSceneNode()->addChild(_ballNode);

  OgreBulletCollisions::SphereCollisionShape *ballShape = 
    new OgreBulletCollisions::SphereCollisionShape(1);

  OgreBulletDynamics::RigidBody *rigidBall = new 
    OgreBulletDynamics::RigidBody("ball", _world);
  rigidBall->setShape(_ballNode, ballShape, 1, 0.05, 10, vball, 
		      Ogre::Quaternion::IDENTITY);


  // Anadimos los objetos a las deques
  _shapes.push_back(ballShape);   _bodies.push_back(rigidBall);








  //---------------------Flecha------------------------------

  flecha = _sceneMgr->createEntity("arrow.mesh");
  nodeflecha = _sceneMgr->createSceneNode("Flecha");
  nodeflecha->attachObject(flecha);
  _sceneMgr->getRootSceneNode()->addChild(nodeflecha);
  //nodeflecha->translate(Ogre::Vector3(0,50,50));
  nodeflecha->setScale(5,5,5);
  //nodeflecha->pitch(Ogre::Degree(60));

/*  OgreBulletCollisions::BoxCollisionShape *flechaShape = 
    new OgreBulletCollisions::BoxCollisionShape(Ogre::Vector3 (1.3,0.35,0.35)); */
  

OgreBulletCollisions::CylinderCollisionShape *flechaShape = 
    new OgreBulletCollisions::CylinderCollisionShape(Ogre::Vector3 (1.5,0.35,0), Ogre::Vector3(0,0,1)); 



  rigidflecha = new OgreBulletDynamics::RigidBody("flechaRigid", _world);
  rigidflecha->setShape(nodeflecha, flechaShape, 0, 0.6, 1,   Ogre::Vector3(0,0,0), Ogre::Quaternion::IDENTITY);
//rigidflecha->disableActiveState();



  // Anadimos los objetos a las deques
  _shapes.push_back(flechaShape);   _bodies.push_back(rigidflecha);




  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
  rigidflecha->disableDeactivation();


  btQuaternion orientacion = btQuaternion (0,-SIMD_2_PI*0.25,0); 
  btVector3 pos = btVector3(0,20,20);


  btTransform trans = btTransform(orientacion,pos);
  rigidflecha->getBulletRigidBody()->proceedToTransform(trans);

   vel = Ogre::Vector3 (0,0,-10);

  auxcamera1=Ogre::Vector3 (0,10,30);


  objetomov = rigidflecha;
}



void
PlayState::exit ()
{
 // _sceneMgr->clearScene();
  //_root->getAutoCreatedWindow()->removeAllViewports();

  // Eliminar cuerpos rigidos --------------------------------------
  std::deque <OgreBulletDynamics::RigidBody *>::iterator 
     itBody = _bodies.begin();
  while (_bodies.end() != itBody) {   
    delete *itBody;  ++itBody;
  } 
 
  // Eliminar formas de colision -----------------------------------
  std::deque<OgreBulletCollisions::CollisionShape *>::iterator 
    itShape = _shapes.begin();
  while (_shapes.end() != itShape) {   
    delete *itShape; ++itShape;
  } 

  _bodies.clear();  _shapes.clear();

  // Eliminar mundo dinamico y debugDrawer -------------------------
  delete _world->getDebugDrawer();    _world->setDebugDrawer(0);
  delete _world;


}

void
PlayState::pause()
{
  _pick=false;
}

void
PlayState::resume()
{
  _pick=true;
  // Se restaura el background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
  Ogre::Vector3 vt(0,0,0);     Ogre::Real tSpeed = 20.0;  

  Ogre::Real deltaT = evt.timeSinceLastFrame;
  int fps = 1.0 / deltaT;

  _world->stepSimulation(deltaT, 1); // Actualizar simulacion Bullet

  posobjetomov = objetomov->getCenterOfMassPosition();




  force+= deltaT;

   _camera->setPosition(posobjetomov + auxcamera1);
  _camera->lookAt(posobjetomov+auxcamera2);


//rigidBodyPlane
  //rigidflecha->get
  //if()

DetectCollision();



  return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  Ogre::Real deltaT = evt.timeSinceLastFrame;
  if (_exitGame)
    return false;
    _world->stepSimulation(deltaT); // Actualizar simulacion Bullet
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P) {
    pushState(PauseState::getSingletonPtr());
  }
  if (e.key == OIS::KC_SPACE) {
    //rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,-9.8,0));
/*
    rigidflecha->applyImpulse (Ogre::Vector3 (0,0,-1), rigidflecha->getCenterOfMassPosition()+Ogre::Vector3 (0,0,3));
  */




  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,-9.8,0));
    rigidflecha->setLinearVelocity(vel);
  rigidflecha->getBulletRigidBody()->setAngularVelocity(btVector3(-0.5,0,0));
  
  }
  if (e.key == OIS::KC_A){
 
  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
    rigidflecha->setLinearVelocity(Ogre::Vector3 (0,0,0));
    //rigidflecha->setDeactivationTime(0);  
  }


  if (e.key == OIS::KC_LEFT){
  btQuaternion orientacion = rigidflecha->getBulletRigidBody()->getOrientation();
  orientacion= orientacion * btQuaternion(0,SIMD_2_PI*0.5*0.1,0);
  btVector3 pos = btVector3(0,20,20); 
  btTransform trans = btTransform(orientacion,pos);
  rigidflecha->getBulletRigidBody()->proceedToTransform(trans);


//btQuaternion aux = btQuaternion(0,0,SIMD_2_PI*0.5*0.1);
  Ogre::Quaternion ori=Ogre::Quaternion (static_cast<Ogre::Radian>(SIMD_2_PI*0.5*0.1),Ogre::Vector3 (0,1,0));
  vel=ori*vel;



  
  }


  
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
  }
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
  //_mouse_position=e.state;
  Ogre::Vector3 vt(0,0,0);     
  Ogre::Real deltaT = 0.02;
  btVector3 pos = btVector3(0,20,20); 

if (_key_pressed){

   btQuaternion orientacion = rigidflecha->getBulletRigidBody()->getOrientation();
   orientacion= orientacion * btQuaternion(0,0,-_mouse_position.X.rel - e.state.X.rel*deltaT);



   btTransform trans = btTransform(orientacion,pos);
   rigidflecha->getBulletRigidBody()->proceedToTransform(trans);
   orientacion= orientacion * btQuaternion(0,-_mouse_position.Y.rel - e.state.Y.rel*deltaT,0);

   trans = btTransform(orientacion,pos);
   rigidflecha->getBulletRigidBody()->proceedToTransform(trans);

   ori=Ogre::Quaternion (static_cast<Ogre::Radian>(-_mouse_position.X.rel - e.state.X.rel*deltaT),Ogre::Vector3 (0,1,0));
   vel=ori*vel;
   auxcamera2+=Ogre::Vector3 (-(-_mouse_position.X.rel - e.state.X.rel*deltaT)*3,0,0);
   ori=Ogre::Quaternion (static_cast<Ogre::Radian>(-_mouse_position.Y.rel - e.state.Y.rel*deltaT),Ogre::Vector3 (1,0,0));
   vel=ori*vel;
   auxcamera2+=Ogre::Vector3 (0,(-_mouse_position.Y.rel - e.state.Y.rel*deltaT)*3,0);


}

}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  if(id==OIS::MB_Middle){
    _key_pressed = true;
    _mouse_position = e.state;
  }
  if(id==OIS::MB_Left){
    force = 0;
  }
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  if(id == OIS::MB_Middle)
     _key_pressed=false;
  if(id==OIS::MB_Left){
    std::cout<<force<<std::endl;
  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,-9.8,0));
    rigidflecha->setLinearVelocity(vel*force*2);
  rigidflecha->getBulletRigidBody()->setAngularVelocity(btVector3(-0.5,0,0));

  }
}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}


void PlayState::DetectCollision() {
  btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
  int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

  for (int i=0;i<numManifolds;i++) {
    btPersistentManifold* contactManifold = 
      bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
    btCollisionObject* obA = 
      const_cast<btCollisionObject*>(contactManifold->getBody0());
    btCollisionObject* obB = 
      const_cast<btCollisionObject*>(contactManifold->getBody1());
    
    Ogre::SceneNode* Plano = _sceneMgr->getSceneNode("bola");
    Ogre::SceneNode* Flecha = _sceneMgr->getSceneNode("Flecha");

    OgreBulletCollisions::Object *obPlano = _world->findObject(Plano);
    OgreBulletCollisions::Object *obFlecha = _world->findObject(Flecha);
    OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
    OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);

    if ((obOB_A == obPlano) || (obOB_B == obPlano)) {
      //Ogre::SceneNode* node = NULL;
      if ((obOB_A != obPlano) && (obOB_A== obFlecha)){
	//node = obOB_A->getRootNode(); 
	std::cout << "COLISION CON EL PLANO" << std::endl;

        RestauraFlecha();
      }
      else if ((obOB_B != obPlano) && (obOB_B== obFlecha)) {
	//node = obOB_B->getRootNode();
	std::cout << "COLISION CON EL PLANO" << std::endl; 
        RestauraFlecha();
      }
      /*if (node) {
	std::cout << node->getName() << std::endl;

      }*/
    } 




  }
}
void PlayState::RestauraFlecha() {
  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
  rigidflecha->disableDeactivation();


  btQuaternion orientacion = btQuaternion (0,-SIMD_2_PI*0.25,0); 
  btVector3 pos = btVector3(0,20,20);


  btTransform trans = btTransform(orientacion,pos);
  rigidflecha->getBulletRigidBody()->proceedToTransform(trans);

   vel = Ogre::Vector3 (0,0,-10);
    rigidflecha->setLinearVelocity(Ogre::Vector3 (0,0,0));
  rigidflecha->getBulletRigidBody()->setAngularVelocity(btVector3(0,0,0));

  auxcamera1=Ogre::Vector3 (0,10,30);
}
