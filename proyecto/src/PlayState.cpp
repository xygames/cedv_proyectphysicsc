#include "PlayState.h"
#include "PauseState.h"

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "Shapes/OgreBulletCollisionsCylinderShape.h"
#include "OgreBulletCollisionsRay.h"
#include "OgreMath.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(0,5,-43));
  _camera->lookAt(Ogre::Vector3(0,5,-10));
  //_camera->setDirection(0,-1,0);
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  // Nuevo background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
  // Creacion del modulo de debug visual de Bullet ------------------
  _debugDrawer = new OgreBulletCollisions::DebugDrawer();
  _debugDrawer->setDrawWireframe(true);	 
  Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->
    createChildSceneNode("debugNode", Ogre::Vector3::ZERO);
  node->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer));

  // Creacion del mundo (definicion de los limites y la gravedad) ---
  Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
    Ogre::Vector3 (-10000, -10000, -10000), 
    Ogre::Vector3 (-10000, -10000, -10000));
  Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,
 	   worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);
  _world->setShowDebugShapes (false);
  auxcamera1=Ogre::Vector3 (0,0,-7);

///Sombras

  _sceneMgr->setAmbientLight(Ogre::ColourValue(0, 0, 0));
  _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

/////////////LUZ//////////////////////

  Ogre::Light* spotLight = _sceneMgr->createLight("SpotLight");
  spotLight->setType(Ogre::Light::LT_POINT);
  spotLight->setDirection(0, -1, -1);
  spotLight->setPosition(Ogre::Vector3(0, 100, -100));

  nflechas = 2;
  createscene();
  flechaready=true;
  _exitGame = false;
}

void
PlayState::exit ()
{
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void
PlayState::pause()
{
}

void
PlayState::resume()
{
  // Se restaura el background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
  Ogre::Real deltaT = evt.timeSinceLastFrame;

  _world->stepSimulation(deltaT, 1); // Actualizar simulacion Bullet
/*
  if(!Rock2rigidBody->getBulletRigidBody()->checkCollideWithOverride(BotonrigidBody->getBulletRigidBody())){
  Abrotrampilla();
  }
*/

  posobjetomov = objetomov->getCenterOfMassPosition();

  Ogre::AxisAlignedBox aab = nodeflecha->_getWorldAABB().intersection(node_globo->_getWorldAABB());
  if(aab.isNull()){
    //std::cout<< "No corta"<<std::endl;  
  }
  else{
    std::cout<< "SI CORTA"<<std::endl;  
    Explotoglobo();
  }
  aab = nodeflecha->_getWorldAABB().intersection(node_entrada->_getWorldAABB());
  if(!aab.isNull()){
    ClavarFlecha(); 
  }
  aab = node_rock1->_getWorldAABB().intersection(node_boton->_getWorldAABB());
  if(!aab.isNull()){
  Abrotrampilla();
  }


  force+= deltaT;

   _camera->setPosition(posobjetomov + auxcamera1);
  _camera->lookAt(posobjetomov);

//DetectCollision();
  if (nflechas<0) lose();
  return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  Ogre::Real deltaT = evt.timeSinceLastFrame;

  _world->stepSimulation(deltaT, 1); // Actualizar simulacion Bullet
  if (_exitGame)
    return false;
  
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P) {
    pushState(PauseState::getSingletonPtr());
  }
  if (e.key == OIS::KC_A) {
    Abrotrampilla();
  }
  if (e.key == OIS::KC_R) {
    RecargoFlecha();
  }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_ESCAPE) {
 // Eliminar cuerpos rigidos --------------------------------------

  //delete ShapePlane,rigidBodyPlane,Rock1Shape,Rock1rigidBody,BaseShape,BaserigidBody;

  // Eliminar mundo dinamico y debugDrawer -------------------------
  delete _world->getDebugDrawer();    _world->setDebugDrawer(0);
  delete _world;
    _exitGame = true;

    
  }
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
 //_mouse_position=e.state;
  Ogre::Vector3 vt(0,0,0);     
  Ogre::Real deltaT = 0.02;
  btVector3 pos = btVector3(0,5,-43); 

if (flechaready){

   btQuaternion orientacion = rigidflecha->getBulletRigidBody()->getOrientation();
   orientacion= orientacion * btQuaternion(0,0,(_mouse_position.X.rel + e.state.X.rel*deltaT)/5);



   btTransform trans = btTransform(orientacion,pos);
   rigidflecha->getBulletRigidBody()->proceedToTransform(trans);
   orientacion= orientacion * btQuaternion(0,(-_mouse_position.Y.rel - e.state.Y.rel*deltaT)/5,0);

   trans = btTransform(orientacion,pos);
   rigidflecha->getBulletRigidBody()->proceedToTransform(trans);

   ori=Ogre::Quaternion (static_cast<Ogre::Radian>(-e.state.X.rel*deltaT)/5,Ogre::Vector3 (0,1,0));
   vel=ori*vel;
   auxcamera1=ori*auxcamera1;
   
   //auxcamera2+=Ogre::Vector3 (-(-_mouse_position.X.rel - e.state.X.rel*deltaT)*3,0,0);
   ori=Ogre::Quaternion (static_cast<Ogre::Radian>(-e.state.Y.rel*deltaT)/5,Ogre::Vector3 (1,0,0));
   vel=ori*vel;
   auxcamera1=ori*auxcamera1;
   //auxcamera2+=Ogre::Vector3 (0,(-_mouse_position.Y.rel - e.state.Y.rel*deltaT)*3,0);


}
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  if(id==OIS::MB_Middle){
    
    _mouse_position = e.state;
  }
  if(id==OIS::MB_Left){
    force = 0;
    flechaready=false;
    nflechas--;
  }
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{

     
  if(id==OIS::MB_Left){
    //vel=Ogre::Vector3(-vel.x,vel.y,vel.z);
    if(force>0.9)
      force=0.9;
    std::cout<<force<<std::endl;
    rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,-9.8,0));
    rigidflecha->setLinearVelocity(vel*force*6);
    rigidflecha->getBulletRigidBody()->setAngularVelocity(btVector3(0.5,0,0));
    auxcamera1=auxcamera1*2 + Ogre::Vector3 (0,10,0);
  }
}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void PlayState::createscene(){
//PLANO
  // Creacion de la entidad y del SceneNode ------------------------
  Ogre::Plane plane1(Ogre::Vector3(0,1,0), 0);    // Normal y distancia
  Ogre::MeshManager::getSingleton().createPlane("p1",
	Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1,
	200, 200, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
  Ogre::SceneNode* node = _sceneMgr->createSceneNode("ground");
  Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt", "p1");
  groundEnt->setMaterialName("ground");
  node->attachObject(groundEnt);
  _sceneMgr->getRootSceneNode()->addChild(node);
  // Creamos forma de colision para el plano ----------------------- 

  ShapePlane = new OgreBulletCollisions::StaticPlaneCollisionShape
    (Ogre::Vector3(0,1,0), 0);   // Vector normal y distancia
  rigidBodyPlane = new OgreBulletDynamics::RigidBody("rigidBodyPlane", _world);
  // Creamos la forma estatica (forma, Restitucion, Friccion) ------
  rigidBodyPlane->setStaticShape(ShapePlane, 0.1, 0.8); 
  
  // Anadimos los objetos Shape y RigidBody ------------------------





//PEDRÁ 1
  Ogre::Vector3 size = Ogre::Vector3::ZERO;	
  Ogre::Vector3 position=Ogre::Vector3::ZERO;
  Ogre::Entity *rock1 = _sceneMgr->createEntity("Rock_1.mesh");
  node_rock1 = _sceneMgr->createSceneNode("Nodo_Piedra");
  node_rock1->attachObject(rock1);
  _sceneMgr->getRootSceneNode()->addChild(node_rock1);
  //node_rock1->showBoundingBox(true);


// Fisica--------------------------------------------
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter3 = new 
    OgreBulletCollisions::StaticMeshToShapeConverter(rock1);
  Rock1Shape = trimeshConverter3->createConvex();
  delete trimeshConverter3;

  Rock1rigidBody = new OgreBulletDynamics::RigidBody("Rock1rigidBody", _world);

  Rock1rigidBody->setShape(node_rock1, Rock1Shape,0.6,0.6,50,Ogre::Vector3(-13,7.5,1.5),Ogre::Quaternion::IDENTITY );
  Rock1rigidBody->getBulletRigidBody()->setGravity(btVector3 (0,0,0));




//BASE BOTON
  Ogre::Entity *base = _sceneMgr->createEntity("Base","Base_boton.mesh");
  Ogre::SceneNode *node_base = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node_base->attachObject(base);

// Fisica-------------------
    Ogre::AxisAlignedBox boundingB = base->getBoundingBox();
    size = boundingB.getSize(); 
    size /= 2.0f;   // El tamano en Bullet se indica desde el centro
    BaseShape = new OgreBulletCollisions::BoxCollisionShape(size);
    BaserigidBody = new OgreBulletDynamics::RigidBody("BaserigidBody",_world);

  BaserigidBody->setShape(node_base, BaseShape,
		     0.6 /* Restitucion */, 0.6 /* Friccion */,
		     0 /* Masa */, Ogre::Vector3(-13,0,1.5) /* Posicion inicial */,
		     Ogre::Quaternion::IDENTITY /* Orientacion */);

//BOTON
  Ogre::Entity *boton = _sceneMgr->createEntity("Boton.mesh");
  node_boton = _sceneMgr->createSceneNode("Nodo_Boton");
  node_boton->attachObject(boton);
  _sceneMgr->getRootSceneNode()->addChild(node_boton);
  node_boton->setScale(1,0.5,1);
  //node_boton->showBoundingBox(true); 	

// Fisica-------------------
    boundingB = boton->getBoundingBox();
    size = boundingB.getSize(); 
    size /= 2.0f;   // El tamano en Bullet se indica desde el centro
    BotonShape = new OgreBulletCollisions::BoxCollisionShape(size);

  BotonrigidBody = new OgreBulletDynamics::RigidBody("BotonrigidBody", _world);

  BotonrigidBody->setShape(node_boton, BotonShape,0.6,0.6,100,Ogre::Vector3(-13,0.5,1.5),Ogre::Quaternion::IDENTITY );

//GLOBO
  Ogre::Entity *globo = _sceneMgr->createEntity("Globo","Balloon.mesh");
  node_globo = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node_globo->attachObject(globo);
  node_globo->setPosition(Ogre::Vector3 (-13,17.36,1.54));
//CUERDA
  Ogre::Entity *cuerda = _sceneMgr->createEntity("Cuerda","Cuerda.mesh");
  node_cuerda = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node_cuerda->attachObject(cuerda);
  node_cuerda->setPosition(Ogre::Vector3 (-13,12.33,1.54));

//PEDRÁ 2

  Ogre::Entity *rock2 = _sceneMgr->createEntity("Piedra2","Rock_2.mesh");
  node_rock2 = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node_rock2->attachObject(rock2);
  //node_rock2->setScale(0.4,0.4,0.4);


// Fisica--------------------------------------------
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = new 
  OgreBulletCollisions::StaticMeshToShapeConverter(rock2);
  Rock2Shape = trimeshConverter->createConvex();
  delete trimeshConverter;

  Rock2rigidBody = new OgreBulletDynamics::RigidBody("Rock2rigidBody", _world);

  Rock2rigidBody->setShape(node_rock2,   Rock2Shape,0.6,0.6,100,Ogre::Vector3(-3.19,10.4,8.04),Ogre::Quaternion::IDENTITY );

  Rock2rigidBody->getBulletRigidBody()->setGravity(btVector3 (0,0,0));

//ENTRADA
  node_entrada = _sceneMgr->createSceneNode("Entrada");
  Ogre::Entity* entrada = _sceneMgr->createEntity("Entrada.mesh");
  node_entrada->attachObject(entrada);
  _sceneMgr->getRootSceneNode()->addChild(node_entrada);
  node_entrada->setScale(1.5,1.5,1.5);
  entrada->setCastShadows(false);
//Fisica---------------------------------
OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter4 = new 
    OgreBulletCollisions::StaticMeshToShapeConverter(entrada);

  trackTrimesh = trimeshConverter4->createTrimesh();

  EntradarigidBody = new 
    OgreBulletDynamics::RigidBody("track", _world);
  EntradarigidBody->setShape(node_entrada, trackTrimesh, 0.8, 0.95, 0, Ogre::Vector3(-3.17,5.78,16.17),Ogre::Quaternion::IDENTITY);

  delete trimeshConverter4;
//Trampilla
  Ogre::SceneNode* node_trampilla = _sceneMgr->createSceneNode("Trampilla");
  Ogre::Entity* trampilla = _sceneMgr->createEntity("Trampilla.mesh");
  node_trampilla->attachObject(trampilla);
  _sceneMgr->getRootSceneNode()->addChild(node_trampilla);
  node_trampilla->setPosition(Ogre::Vector3(-3.33,9.72,8.95));

//FLECHA

  flecha = _sceneMgr->createEntity("arrow.mesh");
  nodeflecha = _sceneMgr->createSceneNode("Flecha");
  nodeflecha->attachObject(flecha);
  //nodeflecha->setScale(50,50,50);
  nodeflecha->setPosition(Ogre::Vector3(0,5,-42));
  _sceneMgr->getRootSceneNode()->addChild(nodeflecha);

//fisica
  OgreBulletCollisions::CylinderCollisionShape *flechaShape = 
    new OgreBulletCollisions::CylinderCollisionShape(Ogre::Vector3 (1.5,0.35,0), Ogre::Vector3(0,0,1)); 
  rigidflecha = new OgreBulletDynamics::RigidBody("flechaRigid", _world);
  rigidflecha->setShape(nodeflecha, flechaShape, 0, 0.6, 0.1,Ogre::Vector3(0,0,0), Ogre::Quaternion::IDENTITY);
  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
  rigidflecha->disableDeactivation();
  
  btQuaternion orientacion = btQuaternion (0,SIMD_2_PI*0.25,0); 
  btVector3 pos = btVector3(0,5,-43);

  btTransform trans = btTransform(orientacion,pos);
  rigidflecha->getBulletRigidBody()->proceedToTransform(trans);

  vel = Ogre::Vector3 (0,0,10);

  objetomov = rigidflecha;

//ENEMY
  Ogre::Entity* yisus = _sceneMgr->createEntity("jesus-base.mesh");
  nodeyisus = _sceneMgr->createSceneNode("Yisus");
  Ogre::Entity* tunica = _sceneMgr->createEntity("Tunica.mesh");
  Ogre::SceneNode* nodetunica = _sceneMgr->createSceneNode("Tunica");
  nodeyisus->attachObject(yisus);
  nodetunica->attachObject(tunica);
  nodeyisus->addChild(nodetunica);
  //nodeflecha->setScale(50,50,50);
  nodeyisus->setPosition(Ogre::Vector3(0,5,-42));
  _sceneMgr->getRootSceneNode()->addChild(nodeyisus);
  nodeyisus->setPosition(Ogre::Vector3(-3.19,0,8.04));
  //nodeyisus->pitch(Ogre::Degree(90));
  //nodeyisus->yaw(Ogre::Degree(180));
  nodeyisus->setScale(3,3,3);

}

void PlayState::Abrotrampilla(){
  _sceneMgr->getSceneNode("Trampilla")->detachAllObjects();
  Rock2rigidBody->getBulletRigidBody()->setGravity(btVector3 (0,-9.8,0));
  objetomov = Rock2rigidBody;
}

/*void PlayState::DetectCollision() {

  btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
  int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();
      std::cout<<numManifolds<<std::endl;
  for (int i=0;i<numManifolds;i++) {
    btPersistentManifold* contactManifold = 
      bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
    btCollisionObject* obA = 
      const_cast<btCollisionObject*>(contactManifold->getBody0());
    btCollisionObject* obB = 
      const_cast<btCollisionObject*>(contactManifold->getBody1());

    Ogre::SceneNode* Roca = _sceneMgr->getSceneNode("Nodo_Piedra");
    Ogre::SceneNode* Boton = _sceneMgr->getSceneNode("Nodo_Boton");

    OgreBulletCollisions::Object *obRoca = _world->findObject(Roca);
    OgreBulletCollisions::Object *obBoton = _world->findObject(Boton);

    OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
    OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);

    if ((obOB_A == obRoca) || (obOB_B== obRoca)){
      if((obOB_A != obRoca) && (obOB_A== obBoton)){
      //Abrotrampilla();
      std::cout<<"SI"<<std::endl;
      }
      else if ((obOB_B != obRoca) && (obOB_B== obBoton)){
      //Abrotrampilla();
      std::cout<<"SI"<<std::endl;
      }


    }
  }
}
*/
void PlayState::Explotoglobo(){
  _sceneMgr->getEntity("Globo")->getParentSceneNode()->detachAllObjects();
  _sceneMgr->getEntity("Cuerda")->getParentSceneNode()->detachAllObjects();
  //_sceneMgr->getEntity("Cuerda");
  Rock1rigidBody->getBulletRigidBody()->setGravity(btVector3 (0,-9.8,0));
  objetomov=Rock1rigidBody;
  auxcamera1=auxcamera1*2 + Ogre::Vector3 (0,10,0);
}

void PlayState::RecargoFlecha() {
  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
  rigidflecha->disableDeactivation();


  btQuaternion orientacion = btQuaternion (0,SIMD_2_PI*0.25,0); 
  btVector3 pos = btVector3(0,5,-43);


  btTransform trans = btTransform(orientacion,pos);
  rigidflecha->getBulletRigidBody()->proceedToTransform(trans);

   vel = Ogre::Vector3 (0,0,10);
    rigidflecha->setLinearVelocity(Ogre::Vector3 (0,0,0));
  rigidflecha->getBulletRigidBody()->setAngularVelocity(btVector3(0,0,0));
  objetomov=rigidflecha;
  auxcamera1=Ogre::Vector3 (0,0,-7);
  flechaready=true;
}

void PlayState::ClavarFlecha(){
   vel = Ogre::Vector3 (0,0,10);
  rigidflecha->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
    rigidflecha->setLinearVelocity(Ogre::Vector3 (0,0,0));
  rigidflecha->getBulletRigidBody()->setAngularVelocity(btVector3(0,0,0));
}

void PlayState::lose(){
  flechaready=false;
  std::cout << "HAS PERDIDO"<<std::endl;


}
